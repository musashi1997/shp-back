using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using SHPAPI.Models;

namespace SHPAPI
{
	public class Startup
	{
		readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }
		
		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddControllers();
			services.AddAutoMapper(typeof(Startup));
			services.AddControllersWithViews()
				.AddNewtonsoftJson(options =>
					options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
				);
			var mapperConfig = new MapperConfiguration(mc =>
			{
				mc.AddProfile(new MappingProfile());
			});
			IMapper mapper = mapperConfig.CreateMapper();
			services.AddSingleton(mapper);
			services.AddDbContext<smartHouseDBContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DatabaseConString")));
			
			// Register the Swagger generator, defining 1 or more Swagger documents
			services.AddSwaggerGen();
			services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
			services.AddSwaggerGen(c =>
			{
				//c.
				c.SwaggerDoc("v1", new OpenApiInfo
				{

					Version = "v1.0.0",
					Title = "Smart House Project API",
					Description = "SHP Swagger API Docs",
					Contact = new OpenApiContact
					{
						Name = "amirmohammad abedini",
						Email = "amirmohammad.abedini@gmail.com",
						Url = new Uri("https://gitlab.com/musashi1997"),
					}


				});

				var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
				var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
				c.IncludeXmlComments(xmlPath);

			});
			services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(o =>
			{

				o.RequireHttpsMetadata = false;
				o.SaveToken = true;
				o.TokenValidationParameters = new TokenValidationParameters
				{
					ValidateIssuer = true,
					ValidateAudience = true,
					ValidateIssuerSigningKey = true,
					ValidateLifetime = true,
					ValidIssuer = Configuration.GetValue<string>("ValidProfiles:ValidIssuer"),
					ValidAudience = Configuration.GetValue<string>("ValidProfiles:ValidAudience"),
					IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration.GetValue<string>("Secrets:IssuerSigningKey"))),
					//ClockSkew = TimeSpan.FromMinutes(5)
				};


				services.AddAuthorization(options =>
				{
					var defaultAuthorizationPolicyBuilder = new AuthorizationPolicyBuilder(
						JwtBearerDefaults.AuthenticationScheme);

					defaultAuthorizationPolicyBuilder =
						defaultAuthorizationPolicyBuilder.RequireAuthenticatedUser();

					options.DefaultPolicy = defaultAuthorizationPolicyBuilder.Build();
				});

			});
			services.AddLettuceEncrypt();
			
			services.AddCors(options =>
			{
				options.AddPolicy(name: MyAllowSpecificOrigins,
					builder =>
					{
						builder.AllowAnyOrigin().AllowAnyHeader()
							.AllowAnyMethod();
					});
			});

		}


		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			app.UseSwagger();

			//app.UseHttpsRedirection();
			app.UseCors(MyAllowSpecificOrigins);
			app.UseRouting();
			app.UseAuthentication();
			app.UseAuthorization();
			app.UseSwaggerUI(c =>
			{
				c.SwaggerEndpoint("/swagger/v1/swagger.json", "APIDocs");

			});
			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});
		}
	}
}

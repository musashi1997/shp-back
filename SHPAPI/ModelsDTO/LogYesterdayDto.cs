﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SHPAPI.ModelsDTO
{
	public class LogYesterdayDto
	{
		public int Temp { get; set; }
		public int Humidity { get; set; }
		public string timeOfDay { get; set; }
	}
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using SHPAPI.Models;

namespace SHPAPI.ModelsDTO
{
	public class UserDTONoPass
	{

		public int UserId { get; set; }
		public string Username { get; set; }
		public string Email { get; set; }
		public string Token { get; set; }
	}
}

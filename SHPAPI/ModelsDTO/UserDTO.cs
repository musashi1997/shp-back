﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SHPAPI.ModelsDTO
{
	public class UserDTO
	{
		public int UserId { get; set; }
		public string Username { get; set; }
		public string Pass { get; set; }
		public string Salt { get; set; }
		[EmailAddress(ErrorMessage = "not a valid email address")]
		public string Email { get; set; }
		public Guid DeviceId { get; set; }
		public string Token { get; set; }
	}
}

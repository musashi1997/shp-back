﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SHPAPI.Models;

namespace SHPAPI.ModelsDTO
{
	public class LogDTO
	{
		public Guid DeviceSerialNum { get; set; }
		public int Temp { get; set; }
		public bool Light { get; set; }
		public int Humidity { get; set; }
		public DateTime DateTime { get; set; }
	}
}

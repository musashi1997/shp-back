﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SHPAPI.ModelsDTO
{
	public class UserLoginDTO
	{
		public string Username { get; set; }
		public string Pass { get; set; }
	}
}

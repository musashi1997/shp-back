﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SHPAPI.ModelsDTO
{
	public class LogBrightnessDTO
	{
		public float wasOn { get; set; }
		public float wasOff { get; set; }
	}
}

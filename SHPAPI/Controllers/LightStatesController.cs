﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using SHPAPI.Models;

namespace SHPAPI.Controllers
{
    [Route("lights")]
    [ApiController]
    public class LightStatesController : ControllerBase
    {
        private readonly smartHouseDBContext _context;

        public LightStatesController(smartHouseDBContext context)
        {
            _context = context;
        }

        /// <summary>
        /// returns all light states (admin only)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult<IEnumerable<LightState>>> GetLightStates()
        {
            return await _context.LightStates.ToListAsync();
        }

        /// <summary>
        /// returns light states to  a user
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        [Authorize(Roles = "user,admin")]
        public async Task<ActionResult<LightState>> GetLightState()
        {
	        var userId = Int32.Parse(this.User.FindFirst("userid").Value);
	        var lightState = await _context.LightStates.Where(u => u.UserId == userId).ToListAsync();

            if (lightState == null)
            {
                return NotFound();
            }

            return Ok(lightState);
        }

        /// <summary>
        /// returns light states of a user (device)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("device/{guid}")]
        public async Task<ActionResult<LightState>> GetLightState(Guid guid)
        {
	        var user = await _context.Users.FirstAsync(a => a.DeviceId == guid);
	        var lightState = await _context.LightStates.Where(u => u.UserId == user.UserId).ToListAsync();

            if (lightState == null)
            {
	            return NotFound();
            }
            var res = from a in lightState
		        select new
		        {
                    state1 = a.L1state,
                    state2 = a.L2state,
                    state3 = a.L3state
		        };
            return Ok(res.First());
        }

        /// <summary>
        /// sets or updates light state
        /// </summary>
        /// <param name="lightState"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "user")]
        public async Task<ActionResult<LightState>> PostLightState(LightState lightState)
        {
	        var userId = Int32.Parse(this.User.FindFirst("userid").Value);
	        if (lightState.UserId != userId)
	        {
		        return Unauthorized();
	        }

	        if (!LightStateExists(lightState.UserId))
	        {
		        _context.LightStates.Add(lightState);
		        await _context.SaveChangesAsync();

		        return Ok(lightState);
	        }
	        var res = await _context.LightStates.SingleOrDefaultAsync(a => a.UserId == userId);
            if (lightState.L1state != null)
            {
	            res.L1state = lightState.L1state;
            }
	        if (lightState.L2state != null)
	        {
		        res.L2state = lightState.L2state;
            }
	        if (lightState.L3state != null)
	        {
		        res.L3state = lightState.L3state;
            }
            
	        
	        _context.Entry(res).State = EntityState.Modified;
	        try 
	        {
			        await _context.SaveChangesAsync();
	        }
	        catch (SqlException e) 
	        { 
		        throw;
	        }

	        return Ok(res);
        }


        private bool LightStateExists(int id)
        {
            return _context.LightStates.Any(e => e.UserId == id);
        }
    }
}

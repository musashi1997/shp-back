﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SHPAPI;
using SHPAPI.Models;
using SHPAPI.ModelsDTO;

namespace SHPAPI.Controllers
{
    [Route("device/logs")]
    [ApiController]
    public class LogsController : ControllerBase
    {
	    private readonly smartHouseDBContext _context;
	    private readonly IMapper _mapper;
	    private readonly Random _random = new Random();
	    private readonly IConfiguration config;
	    public LogsController(smartHouseDBContext context, IMapper mapper, IConfiguration config)
	    {
		    this._mapper = mapper;
		    this._context = context;
		    this.config = config;
	    }
	   /// <summary>
	   /// post data from device
	   /// </summary>
	   /// <param name="logDto"></param>
	   /// <returns></returns>
	    [HttpPost]
	    [AllowAnonymous]
	    public async Task<ActionResult<Log>> PostLog(LogDTO logDto)
	    {

		    var log = _mapper.Map<Log>(logDto);
		    log.DateTime = DateTime.Now;
		    var user = await _context.Users.Where(i => i.DeviceId == logDto.DeviceSerialNum).FirstAsync();
		    if (user == null)
		    {
			    return BadRequest("user not found");
		    }
		    log.UserId = user.UserId;

		    _context.Logs.Add(log);
		    await _context.SaveChangesAsync();

		    return Ok();
	    }

		/// <summary>
		/// gets all user logs (admin debug tool)
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		[Authorize(Roles = "admin")]
        public async Task<ActionResult<IEnumerable<Log>>> GetLogs()
        {
            return await _context.Logs.ToListAsync();
        }

		/// <summary>
		/// get a single log by id (admin)
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
        [HttpGet("{id}")]
        [Authorize(Roles = "admin")]
		public async Task<ActionResult<Log>> GetLog(int id)
        {
            var log = await _context.Logs.FindAsync(id);

            if (log == null)
            {
                return NotFound();
            }
            return log;
        }

		/// <summary>
		/// get latest user log (user)
		/// </summary>
		/// <returns></returns>
        [HttpGet("now")]
        [Authorize(Roles = "user")]
		public async Task<ActionResult<Log>> GetLogNow()
        {
	        var id = Int32.Parse(this.User.FindFirst("userid").Value);
	        var log = await _context.Logs.Where(a => a.UserId == id).OrderBy(a => a.DateTime).LastAsync();

	        if (log == null)
	        {
		        return NotFound();
	        }
	        return log;
        }

		/// <summary>
		/// get yesterday brightness percentage (user)
		/// </summary>
		/// <param name="query"></param>
		/// <returns></returns>
		[Authorize(Roles = "user")]
		[HttpGet("yesterday/brightness")]
		public async Task<ActionResult<IEnumerable<LogYesterdayDto>>> GetYesterdayBrightness([FromQuery]string query)
		{
			var id = Int32.Parse(this.User.FindFirst("userid").Value);

			var log = await _context.Logs.Where(l => l.UserId == id).Where(l => l.DateTime > DateTime.Today.AddDays(-1) && l.DateTime < DateTime.Today).ToListAsync();

			if (log == null)
			{
				return NotFound();
			}
			var res = new LogBrightnessDTO();
			int wasOn = 0, wasOff = 0, i = 0;
			foreach (var l in log)
			{
				if (l.Light)
				{
					wasOn++;
				}
				else
				{
					wasOff++;
				}
			}

			res.wasOn = (float)Math.Round(((float) wasOn / log.Count )* 100f) / 100f;
			res.wasOff = (float)Math.Round(((float)wasOff / log.Count) * 100f) / 100f;
			return Ok(res);
		}

		/// <summary>
		/// gets average temp & humidity of yesterday
		/// time of day must be given in the query parameter (morning, noon, evening or night)
		/// </summary>
		/// <param name="query"></param>
		/// <returns></returns>
		[Authorize(Roles = "user,admin")]
        [HttpGet("yesterday")]
        public async Task<ActionResult<IEnumerable<LogYesterdayDto>>> GetLogsYesterday([FromQuery]string query)
        {
	        var id = Int32.Parse(this.User.FindFirst("userid").Value);
	        var log = await _context.Logs.Where(l => l.UserId == id).Where(l => l.DateTime > DateTime.Today.AddDays(-1) && l.DateTime < DateTime.Today).ToListAsync();

	        if (log == null)
	        {
		        return NotFound();
	        }

	        int tempSum = 0, humiditySum =0 , i =0;
	        var res = new LogYesterdayDto();
	        switch (query)
	        {
                case "morning":
	                foreach (var l in log)
	                {
		                if (l.DateTime.Hour>=02 && l.DateTime.Hour<=11)
		                {
			                tempSum += l.Temp;
			                humiditySum += l.Humidity;
			                i++;
		                }
	                }
	                if (i == 0)
	                {
		                i = 1;
	                }
					res.Humidity = humiditySum / i;
	                res.Temp = tempSum / i;
	                res.timeOfDay = "morning";
                    break;
                case "noon":
	                foreach (var l in log)
	                {
		                if (l.DateTime.Hour >= 11 && l.DateTime.Hour <= 17)
		                {
			                tempSum += l.Temp;
			                humiditySum += l.Humidity;
			                i++;
		                }
	                }
	                if (i == 0)
	                {
		                i = 1;
	                }
					res.Humidity = humiditySum / i;
	                res.Temp = tempSum / i;
	                res.timeOfDay = "noon";
                    break;
                case "evening":
	                foreach (var l in log)
	                {
		                if (l.DateTime.Hour >= 17 && l.DateTime.Hour <= 20)
		                {
			                tempSum += l.Temp;
			                humiditySum += l.Humidity;
			                i++;
		                }
	                }
	                if (i == 0)
	                {
		                i = 1;
	                }
					res.Humidity = humiditySum / i;
	                res.Temp = tempSum / i;
	                res.timeOfDay = "evening";
	                break;
                case "night":
	                foreach (var l in log)
	                {
		                if (l.DateTime.Hour >= 20 && l.DateTime.Hour <= 23)
		                {
			                tempSum += l.Temp;
			                humiditySum += l.Humidity;
			                i++;
		                }
	                }

	                if (i == 0)
	                {
		                i = 1;
	                }
					res.Humidity = humiditySum / i;
	                res.Temp = tempSum / i;
	                res.timeOfDay = "night";
					break;
                default:
	                return BadRequest();
            }

	        return Ok(res);
        }

		/// <summary>
		/// returns all logs of today (user)
		/// </summary>
		/// <returns></returns>
        [Authorize(Roles = "user")]
        [HttpGet("today")]
        public async Task<ActionResult<IEnumerable<Log>>> GetLogsToday()
        {
	        var id = Int32.Parse(this.User.FindFirst("userid").Value);
			var log = await _context.Logs.Where(l => l.UserId==id).Where(l => l.DateTime > DateTime.Today).ToListAsync();

	        if (log == null)
	        {
		        return NotFound();
	        }

	        return log;
        }

		/// <summary>
		///  returns all logs of last month (user)
		/// </summary>
		/// <returns></returns>
		[Authorize(Roles = "user")]
        [HttpGet("tomonth")]
        public async Task<ActionResult<IEnumerable<Log>>> GetLogsThisMonth()
        {
	        var id = Int32.Parse(this.User.FindFirst("userid").Value);
			var log = await _context.Logs.Where(l => l.UserId == id).Where(l => l.DateTime > DateTime.Now.AddMonths(-1)).ToListAsync();

	        if (log == null)
	        {
		        return NotFound();
	        }

	        return log;
        }

		/// <summary>
		/// returns all logs of last year (user)
		/// </summary>
		/// <returns></returns>
		[Authorize(Roles = "user")]
        [HttpGet("toyear")]
        public async Task<ActionResult<IEnumerable<Log>>> GetLogsThisYear()
        {
	        var id = Int32.Parse(this.User.FindFirst("userid").Value);
			var log = await _context.Logs.Where(l => l.UserId == id).Where(l => l.DateTime > DateTime.Now.AddYears(-1)).ToListAsync();

	        if (log == null)
	        {
		        return NotFound();
	        }

	        return log;
        }


        /// <summary>
		/// update a log
		/// </summary>
		/// <param name="id"></param>
		/// <param name="log"></param>
		/// <returns></returns>
        [HttpPut("{id}")]
		[Authorize(Roles = "user")]
        public async Task<IActionResult> PutLog(int id, Log log)
        {
	        var userid = Int32.Parse(this.User.FindFirst("userid").Value);
	        if (log.UserId != userid)
	        {
		        return Unauthorized();
	        }

			if (id != log.LogId)
            {
                return BadRequest();
            }

            _context.Entry(log).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }

            catch (DbUpdateConcurrencyException)
            {
                if (!LogExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }


        /// <summary>
		/// delete a log by it's id(admin)
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
        [HttpDelete("{id}")]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult<Log>> DeleteLog(int id)
        {
            var log = await _context.Logs.FindAsync(id);
            if (log == null)
            {
                return NotFound();
            }

            _context.Logs.Remove(log);
            await _context.SaveChangesAsync();

            return log;
        }

        private bool LogExists(int id)
        {
            return _context.Logs.Any(e => e.LogId == id);
        }
    }
}

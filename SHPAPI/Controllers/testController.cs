﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SHPAPI;
using SHPAPI.Models;

namespace SHPAPI.Controllers
{
    [Route("test")]
    [ApiController]
    [Authorize]
    public class testController : ControllerBase
    {

		/// <summary>
		/// This is a testing purpose only controller
		/// </summary>
		/// <returns>string</returns>
		[HttpGet]
		[AllowAnonymous]
		public async Task<IActionResult> TestCtrl()
		{
			return Ok("This is a test Controller");
		}

		/// <summary>
		/// Gets and returns server time
		/// </summary>
		/// <returns></returns>
		[HttpGet("time")]
		[AllowAnonymous]
		public async Task<IActionResult> GetTime()
		{
			return Ok(DateTime.Now);
		}
	}
}

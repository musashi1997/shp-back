﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SHPAPI.Models;
using SHPAPI.ModelsDTO;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;

using System.ComponentModel.DataAnnotations;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace SHPAPI.Controllers
{
	[Authorize]
	[ApiController]
	[Route("Users")]
	public class UsersController : Controller
	{
		private readonly smartHouseDBContext _context;
		private readonly IMapper _mapper;
		private readonly Random _random = new Random();
		private readonly IConfiguration _config;
		public UsersController(smartHouseDBContext context, IMapper mapper, IConfiguration config)
		{
			this._mapper = mapper;
			this._context = context;
			this._config = config;
		}

		/// <summary>
		/// user login controller
		/// </summary>
		/// <param name="userDto"></param>
		/// <returns></returns>
		[AllowAnonymous]
		[HttpPost("authenticate")]
		public async Task<ActionResult<User>> Authenticate([FromBody]UserLoginDTO userDto)
		{
			if (string.IsNullOrEmpty(userDto.Username) || string.IsNullOrEmpty(userDto.Pass))
			{
				return BadRequest("username or pass is null or empty");
			}
			var user = await _context.Users.SingleOrDefaultAsync(x => x.Username == userDto.Username);
			if (user == null)
				return BadRequest(new { message = "Username or Pass is incorrect" });

			if (!VerifyPassHash(userDto.Pass, user.Pass, user.Salt))
			{
				return BadRequest(new { message = "Username or Pass is incorrect" });
			}


			var tokenHandler = new JwtSecurityTokenHandler();
			var key = Encoding.ASCII.GetBytes(_config.GetValue<string>("Secrets:IssuerSigningKey"));
			var tokenDescriptor = new SecurityTokenDescriptor
			{
				Issuer = _config.GetValue<string>("ValidProfiles:ValidIssuer"),
				Audience = _config.GetValue<string>("ValidProfiles:ValidAudience"),
				Subject = new ClaimsIdentity(new Claim[]
				{
					new Claim(ClaimTypes.Role, "user"),
					new Claim("username", user.Username),
					new Claim("userid",user.UserId.ToString())
				}),
				Expires = DateTime.UtcNow.AddDays(30),
				SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
			};

			var token = tokenHandler.CreateToken(tokenDescriptor);
			var tokenString = tokenHandler.WriteToken(token);
			UserDTONoPass userTokenHolder = _mapper.Map<UserDTONoPass>(user);
			
			userTokenHolder.Token = tokenString;

			return Ok(userTokenHolder);

		}
		/// <summary>
		/// registers a user
		/// </summary>
		/// <param name="userDto"></param>
		/// <returns></returns>
		[AllowAnonymous]
		[HttpPost("register")]
		public async Task<ActionResult<User>> Register([FromBody]UserDTO userDto)
		{
			if (_context.Users.Any(x => x.Username == userDto.Username))
			{
				return BadRequest("username already taken");
			}
			if (_context.Users.Any(x => x.Email == userDto.Email))
			{
				return BadRequest("Email Address already taken");
			}

			var device = await _context.Devices.FindAsync(userDto.DeviceId);
			if (device == null || device.IsBound == true )
			{
				return BadRequest("Entered device code is not valid");
			}
			
			byte[] PassHash, PassSalt;
			CreatePassHash(userDto.Pass, out PassHash, out PassSalt);

			User user = _mapper.Map<User>(userDto);

			user.Pass = PassHash;
			user.Salt = PassSalt;
			device.IsBound = true;
			await _context.Users.AddAsync(user);
			_context.Entry(device).State = EntityState.Modified;
			await _context.SaveChangesAsync();
			return Ok($"user: {user.Username} successfully created");

		}
		
		/// <summary>
		/// gets user by provided user id (
		/// </summary>
		/// <param name="userId"></param>
		/// <returns></returns>
		[Authorize(Roles = "user")]
		[HttpGet("{id}")]
		public async Task<ActionResult<User>> getUserbyId([FromRoute(Name = "id")]int userId)
		{
			if (Int32.Parse(this.User.FindFirst("userid").Value) != userId)
			{
				return Unauthorized();
			}
			var user = await _context.Users.FindAsync(userId);
			if (user == null)
			{
				return BadRequest("user not found");
			}

			UserDTONoPass u = _mapper.Map<UserDTONoPass>(user);

			return Ok(u);
		}

		/// <summary>
		/// updates user profile
		/// </summary>
		/// <param name="id"></param>
		/// <param name="userDto"></param>
		/// <returns></returns>
		[Authorize(Roles = "user,admin")]
		[HttpPut("{id}")]
		public async Task<ActionResult<User>> UpdateUser(int id, [FromBody]UserDTO userDto)
		{
			if (Int32.Parse(this.User.FindFirst("userid").Value) != id)
			{
				return Unauthorized();
			}
			User newUser = _mapper.Map<User>(userDto);
			var  dbExistingUser = await _context.Users.FindAsync(id);
			if (dbExistingUser == null)
			{
				return NotFound();
			}
			if (dbExistingUser.Username != newUser.Username)
			{
				if (_context.Users.Any(x => x.Username == newUser.Username))
				{
					return BadRequest("this username is taken");
				}
			}
			if (dbExistingUser.Email != newUser.Email)
			{
				if (_context.Users.Any(x => x.Email == newUser.Email))
				{
					return BadRequest("this email is taken");
				}
			}
			dbExistingUser.Username = newUser.Username;
			dbExistingUser.Email = newUser.Email;
			
			if (!string.IsNullOrWhiteSpace(userDto.Pass))
			{
				byte[] PassHash, PassSalt;
				CreatePassHash(userDto.Pass, out PassHash, out PassSalt);

				dbExistingUser.Pass = PassHash;
				dbExistingUser.Salt = PassSalt;
				_context.Users.Update(dbExistingUser);
				await _context.SaveChangesAsync();
				return Ok();
			}
			return BadRequest("white password");
		}
		

		private static void CreatePassHash(string Pass, out byte[] PassHash, out byte[] PassSalt)
		{
			if (Pass == null) throw new ArgumentNullException("Pass empty");
			if (string.IsNullOrWhiteSpace(Pass)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "Pass");

			using (var hmac = new System.Security.Cryptography.HMACSHA512())
			{
				PassSalt = hmac.Key;
				PassHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(Pass));
			}
		}


		private static bool VerifyPassHash(string Pass, byte[] storedHash, byte[] storedSalt)
		{
			if (Pass == null) throw new ArgumentNullException("Pass");
			if (string.IsNullOrWhiteSpace(Pass)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "Pass");
			if (storedHash.Length != 64) throw new ArgumentException("Invalid length of Pass hash (64 bytes expected is " + storedHash.Length + " " + ").", "PassHash");
			if (storedSalt.Length != 128) throw new ArgumentException("Invalid length of Pass salt (128 bytes expected is " + storedSalt.Length + " ).", "PassHash");

			using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
			{
				var computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(Pass));

				for (int i = 0; i < computedHash.Length; i++)
				{
					if (computedHash[i] != storedHash[i]) return false;
				}

			}

			return true;
		}
	}
}
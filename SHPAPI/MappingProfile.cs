﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using SHPAPI.Models;
using SHPAPI.ModelsDTO;

namespace SHPAPI
{
	public class MappingProfile : Profile
	{
		public MappingProfile()
		{
			CreateMap<User, UserDTO>();
			CreateMap<UserDTO, User>().ForMember(c => c.Pass,
				option => option.Ignore());
			CreateMap<User, UserDTONoPass>();
			CreateMap<UserDTONoPass, User>();
			CreateMap<UserDTONoPass, User>();
			CreateMap<User, UserDTONoPass>();
			//logs
			CreateMap<Log , LogDTO>().ForMember(c => c.DeviceSerialNum,
				option => option.Ignore()); //todo ignore guid field
			CreateMap<LogDTO, Log>();
		}
	}
}
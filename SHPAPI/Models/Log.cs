﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SHPAPI.Models
{
    public partial class Log
    {
        public int LogId { get; set; }
        public int UserId { get; set; }
        public int Temp { get; set; }
        public bool Light { get; set; }
        public int Humidity { get; set; }
        public DateTime DateTime { get; set; }

        public virtual User User { get; set; }
    }
}

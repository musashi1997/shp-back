﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SHPAPI.Models
{
    public partial class Device
    {
        public Guid DeviceId { get; set; }
        public bool IsBound { get; set; }
    }
}

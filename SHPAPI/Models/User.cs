﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SHPAPI.Models
{
    public partial class User
    {
        public User()
        {
            LightStates = new HashSet<LightState>();
            Logs = new HashSet<Log>();
        }

        public int UserId { get; set; }
        public string Username { get; set; }
        public byte[] Pass { get; set; }
        public byte[] Salt { get; set; }
        public string Email { get; set; }
        public bool IsVerified { get; set; }
        public bool? HeaterOn { get; set; }
        public bool? CoolerOn { get; set; }
        public Guid? DeviceId { get; set; }

        public virtual ICollection<LightState> LightStates { get; set; }
        public virtual ICollection<Log> Logs { get; set; }
    }
}

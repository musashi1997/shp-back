﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SHPAPI.Models
{
    public partial class LightState
    {
        public int LightId { get; set; }
        public int UserId { get; set; }
        public bool? L1state { get; set; }
        public bool? L2state { get; set; }
        public bool? L3state { get; set; }

        public virtual User User { get; set; }
    }
}
